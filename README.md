

## Homework for an interview
### Requirement
https://gitlab.com/brainsoft-eu/python-ai-code-challenge

### Solution

#### What I have done
- Create an application that allows the user to query our SDK documentation.
  - The agent remembers conversation context.
  - Every response related to the SDK documentation must contain sources (relevant links to the documentation page).
- Introduce UI for communication with the agent (React)
  - Handle edge cases and crashes.
- (optional) The agent can lookup for specific facts on the web (DuckDuckGo).
- (optional) The agent can execute Python code.
- (optional) One can interact with the agent via API (Rest/gRPC), see http://127.0.0.1:8000/docs#/default/message_message_post

#### What I have not done
- The agent handles large conversations.
- (optional) Use streaming for responses.
- (optional) Allow the user to upload file(s). 
- (optional) The agent can work with files (CSV / PDF).
- (optional) One can interact with the agent via CLI.

#### What to do next
- Use logger `logger = logging.getLogger(__name__)` instead of `print`
- Separate vectordb into a separate service.
- Separate chat_history to a separate DB, e.g. SQL DB
- Two steps above make FastApi server stateless and ready to be scaled.
- Split the articles of Docs by section with id, or h1, h2, h3... (There is a link for each)

## Start Chatbot

### Create Py env
- create/activate python env (Python 3.10)
- install requirements `pip install -r requirements.txt`

### Prepare vectordb
- setup env `OPENAI_API_KEY`
- go to `backend` directory
- run `python load_docs.py`

### Run BE server
- setup env `OPENAI_API_KEY`
- go to `backend` directory
- run `python -m uvicorn server:app`

### Run FE server
- go to `frontend` directory
- install dependencies `npm install`
- run `npm start`
