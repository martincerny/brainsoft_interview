from langchain import hub
from langchain.agents import AgentExecutor, create_openai_functions_agent
from langchain.tools.retriever import create_retriever_tool
from langchain_community.tools.ddg_search import DuckDuckGoSearchRun
from langchain_community.vectorstores.chroma import Chroma
from langchain_experimental.tools import PythonREPLTool
from langchain_openai import ChatOpenAI, OpenAIEmbeddings

from config import VECTOR_DB_PATH


# creating tools
def create_retriever():
    embeddings_model = OpenAIEmbeddings()
    vectordb = Chroma(persist_directory=VECTOR_DB_PATH, embedding_function=embeddings_model)
    print(f"Loaded {len(vectordb.get()['documents'])} documents from Chroma DB.")
    retriever = vectordb.as_retriever()
    # todo custom tool
    return retriever


def create_agent():
    retriever_tool = create_retriever_tool(
        create_retriever(),
        "ibm_generative_ai_search",
        "Search for information about IBM Generative AI Python SDK."
        " For any questions about IBM Generative AI, you must use this tool!",
    )
    tools = [DuckDuckGoSearchRun(), retriever_tool, PythonREPLTool()]
    llm = ChatOpenAI(model="gpt-3.5-turbo", temperature=0)
    prompt = hub.pull("hwchase17/openai-functions-agent")

    agent = AgentExecutor(
        agent=create_openai_functions_agent(llm, tools, prompt),
        tools=tools,
        verbose=True,
    )
    print(f"Created agent executor {agent}")
    return agent
