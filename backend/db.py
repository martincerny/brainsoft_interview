from uuid import uuid4
from typing import List

from langchain_core.messages import BaseMessage


class ChatHistoryStore:
    def __init__(self):
        self._history = {}

    def exists(self, chat_id: str) -> bool:
        return chat_id in self._history

    def get_history(self, chat_id: str) -> List[BaseMessage]:
        print(f"Getting history for chat {chat_id}")
        return self._history[chat_id]

    def create_new_chat_id(self) -> str:
        """
        Returns the new chat id which is unique
        """
        new_id = str(uuid4())
        while new_id in self._history:
            new_id = str(uuid4())
        print(f"Creating new chat with id={new_id}")
        self._history[new_id] = []
        return new_id

    def append_message(self, chat_id: str, message: BaseMessage):
        print(f"Appending message {message} to chat {chat_id}")
        self._history[chat_id].append(message)
