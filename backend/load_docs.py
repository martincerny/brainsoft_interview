from bs4 import BeautifulSoup
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import RecursiveUrlLoader
from langchain_community.embeddings import OpenAIEmbeddings
from langchain_community.vectorstores.chroma import Chroma

from backend.config import VECTOR_DB_PATH

# 1. Load and transform
url = "https://ibm.github.io/ibm-generative-ai/"
xpath_article_query = '//*/article[@role="main"]'


def exctract_article(data):
    soup = BeautifulSoup(data, "html.parser")

    article_result_set = soup.find_all("article", {"role": "main"})
    if article_result_set:
        return article_result_set[0].text


loader = RecursiveUrlLoader(url=url, max_depth=2, extractor=exctract_article)

docs = loader.load()
# chunks can be changed, just first try
documents = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=200).split_documents(docs)


# 2. Embed and store to vector DB
embeddings_model = OpenAIEmbeddings()
db = Chroma.from_documents(documents, embeddings_model, persist_directory=VECTOR_DB_PATH)

print(f"Saved {len(db.get()['documents'])} documents to Chroma DB.")
