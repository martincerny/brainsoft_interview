from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class MessagePayloadRequest(BaseModel):
    text: str
    session_id: Optional[str] = None
    class Config:
        schema_extra = {}


class MessagePayloadResponse(BaseModel):
    text: str
    session_id: str
    timestamp: datetime
