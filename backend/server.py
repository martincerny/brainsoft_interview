from datetime import datetime
from typing import List

from fastapi import FastAPI
from langchain_core.messages import BaseMessage, HumanMessage, AIMessage
from fastapi.middleware.cors import CORSMiddleware

from bot import create_agent
from db import ChatHistoryStore
from models import MessagePayloadRequest, MessagePayloadResponse


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

agent = create_agent()
chat_history_store = ChatHistoryStore()


@app.get("/health")
def health():
    return "ok"


@app.post("/message")
async def message(message: MessagePayloadRequest) -> MessagePayloadResponse:
    print(f"Recieved message {message}")
    if not chat_history_store.exists(message.session_id):
        # first message in new session
        message.session_id = chat_history_store.create_new_chat_id()
        chat_history = []
    else:
        # get history of current session
        chat_history: List[BaseMessage] = chat_history_store.get_history(message.session_id)

    # use agent
    payload = {
        "input": message.text,
        "chat_history": chat_history,
    }
    print(f"Using agent with payload {payload}")
    agent_response = await agent.ainvoke(payload)
    print(f"Agent responded with {agent_response}")
    agent_response_output = agent_response["output"]
    # save messages to history storage
    chat_history_store.append_message(message.session_id, HumanMessage(content=message.text))
    chat_history_store.append_message(message.session_id, AIMessage(content=agent_response_output))

    response = MessagePayloadResponse(text=agent_response_output, session_id=message.session_id, timestamp=datetime.now())
    return response
