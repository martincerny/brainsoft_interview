import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

interface Message {
  text: string;
  sender: 'user' | 'bot';
}

const App: React.FC = () => {
  const [sessionID, setSessionId] = useState<string>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [messages, setMessages] = useState<Message[]>([]);
  const [inputText, setInputText] = useState<string>('');

  const handleSendMessage = async () => {
    if (inputText.trim() === '') return;

    const userMessage: Message = {
      text: inputText,
      sender: 'user'
    }
    setIsLoading(true);
    try {
        const response = await sendMessage(inputText, sessionID);
        if (response.session_id !== sessionID) {
          setSessionId(response.session_id)
        }

        const newMessage: Message = {
          text: response.text,
          sender: 'bot'
        };

        setMessages([...messages, userMessage, newMessage]);
        setInputText('');
    } catch (e) {
        console.log('Got exception: ', e)
    }
    setIsLoading(false);
  };

  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col">
          <h3>Chat App</h3>
          <small>Session ID: {sessionID}</small>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <div className="overflow-auto" style={{ height: 'auto' }}>
                {messages.map((message, index) => (
                  <div
                    key={index}
                    className={`message-${message.sender} mb-2`}
                  >
                    <strong>{message.sender === 'user' ? 'You' : 'Bot'}:</strong>{' '}
                      {message.text.split("\n").map(str => <p>{str}</p>)}
                  </div>
                ))}
              </div>
              <div className="mt-3">
                <div className="input-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Type your message..."
                    value={inputText}
                    disabled={isLoading}
                    onChange={(e) => setInputText(e.target.value)}
                    onKeyDown={(e) => {
                      if (e.key === 'Enter') {
                        handleSendMessage()
                      }
                    }}
                  />
                  <button
                    className="btn btn-primary"
                    disabled={isLoading}
                    onClick={handleSendMessage}
                  >
                    Send
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


const sendMessage = async (text: string, session_id: string) => {
    const location = window.location.hostname;
    const config = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({text, session_id})
    };
    const response = await fetch(`http://${location}:8000/message`, config);
    if (response.ok) {
        return await response.json();
    } else {
        throw new Error(`Got response with status_code ${response.status}`);
    }
}

export default App;
